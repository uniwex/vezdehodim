$( document ).ready(function() {

    $('#saleform-price').on('change',function () {
        $('#sum-value').empty();
        $('#sum-spinner').show();
        if ($(this).val() == 0) {
            $('#sum-spinner').hide();
            $('#sum-value').html('0');
        }
        $.post('/sale/price?id='+$(this).val(), function (data) {
            $('#sum-spinner').hide();
            $('#sum-value').html(data*$('#qty').val());
        });
        $.post('/sale/left?id='+$(this).val(), function (data) {
            $('#qty').attr('max', data);
        });
    });

    $('#qty').on('change',function () {

        $('#sum-value').empty();
        $('#sum-spinner').show();
        if ($('#saleform-price').val() == 0) {
            $('#sum-spinner').hide();
            $('#sum-value').html('0');
        }
        $.post('/sale/price?id='+$('#saleform-price').val(), function (data) {
            $('#sum-spinner').hide();
            $('#sum-value').html(data*$('#qty').val());
        });
    });

    $('#qty').on('keyup',function () {
        /*if (isFinite($(this).val()) == false)
        {
            alert($(this).val());
        }*/
        $('#sum-value').empty();
        $('#sum-spinner').show();
        if ($('#saleform-price').val() == 0) {
            $('#sum-spinner').hide();
            $('#sum-value').html('0');
        }
        $.post('/sale/price?id='+$('#saleform-price').val(), function (data) {
            $('#sum-spinner').hide();
            $('#sum-value').html(data*$('#qty').val());
        });
    });

    $('#qty').on('keypress', function (e) {

    });
});