<?php
namespace frontend\controllers;


use yii\web\Controller;
use common\models\Event;
use common\models\Image;
use common\models\Category;

/**
 * Site controller
 */
class DetailsController extends Controller
{

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex($id)
    {
        $event = Event::findOne($id);
        $category = Category::find()->all();
        $image = Image::find()->joinWith(['eventImg i'])->where(['i.eventID' => $id])->all();

        return $this->render('index', [
            'model' => $event,
            'images' => $image,
            'category' => $category,
        ]);
    }

}
