<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use common\models\Event;
use common\models\Price;
use common\models\SaleRequest;
use common\models\SoldTickets;
use frontend\models\SaleForm;

/**
 * Site controller
 */
class SaleController extends Controller
{
    public $enableCsrfValidation = false;
    public $secret = 'vyp1kJ4esGbyJobVh+yfRRM7';
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex($id)
    {
        $model = new SaleForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if (Yii::$app->request->post()['SaleForm']['price'] > 0)
                $price = Price::findOne(Yii::$app->request->post()['SaleForm']['price']);
            else
            {
                $event = Event::findOne($id);
                $model->phone = Yii::$app->request->post()['SaleForm']['phone'];
                return $this->render('index', [
                    'model' => $model,
                    'event' => $event,
                ]);
            }
            if ($price->left < Yii::$app->request->post()['SaleForm']['qty']){
                $event = Event::findOne($id);
                $model->phone = Yii::$app->request->post()['SaleForm']['phone'];
                return $this->render('index', [
                    'model' => $model,
                    'event' => $event,
                ]);
            }
            $qty = Yii::$app->request->post()['SaleForm']['qty'];
            $sum = $price->value * $qty;
            $ytype = Yii::$app->request->post()['ytype'];
            $label = json_encode(Yii::$app->request->post()['SaleForm']);
            $url = "https://money.yandex.ru/quickpay/confirm.xml?receiver=410014217308373&quickpay-form=small&referer&is-inner-form=true&targets=Покупка билета&sum=$sum&maxlength=8&successURL=tickets.red-flag.ru%2Fsite%2Findex&paymentType=".$ytype."&label=$label";
            return $this->redirect($url);
            /*$event = Event::findOne($id);
            return $this->render('index', [
                'model' => $model,
                'url' => $url,
                'event' => $event,
            ]);*/
        }
        else{
            $event = Event::findOne($id);
            return $this->render('index', [
                'model' => $model,
                'event' => $event,
            ]);
        }
    }

    public function actionPrice($id)
    {
        $price = Price::findOne($id);
        echo $price->value;
    }

    public function actionLeft($id)
    {
        $price = Price::findOne($id);
        echo $price->left;
    }

    public function actionIncoming(){
        if (isset($_POST)){
            $post = print_r($_POST, true);
            $request = new SaleRequest();
            $request->paymentID = 1;
            $request->request = json_encode($_POST);
            $request->save();
            $label = json_decode($_POST['label']);
            $price = Price::findOne($label->price);
            $priceValue = $price->value;
            $amountValue = $_POST['withdraw_amount']/$label->qty;
            for ($i = 1; $i <= $label->qty; $i++){
                $sold = new SoldTickets();
                $sold->priceID = $label->price;
                $sold->phone = $label->phone;
                $hash = $sold->generateHash($label->price);
                $sold->hash = $hash;
                $sold->priceValue = $priceValue;
                $sold->amountValue = $amountValue;
                $sold->save();

                // ОТПРАВКА СМС
                Yii::$app->sms->sms_send( $label->phone, 'Ваш билет: '.$hash );

            }
            /*$sha1 = sha1($post['notification_type'].'&'.$post['operation_id'].'&'.$post['amount'].'&'.$post['currency'].'&'.$post['datetime'].'&'.$post['sender'].'&'.$post['codepro'].'&'.$this->secret.'&'.$post['label']);
            $post['calc'] = $sha1;
            if ($post['sha1_hash'] == $sha1)
                $post['success'] = 'true';*/
            file_put_contents('sale.log', $post);
        }
    }

    public function actionOut()
    {
        return $this->render('test');
    }

    public function actionHash($id)
    {
        echo (new SoldTickets())->generateHash($id);
    }
}
