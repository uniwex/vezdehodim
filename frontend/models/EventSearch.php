<?php

namespace frontend\models;

use common\models\Event;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\db\Expression;

/**
 * EventSearch represents the model behind the search form about `common\models\Event`.
 */
class EventSearch extends Event
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'companyID', 'catID'], 'integer'],
            [['name', 'description', 'preview', 'icon'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $expression = new Expression('NOW()');
        $query = Event::find()->where('`eDate` > '.$expression);
        if (isset($_GET['companyID'])){
            $query->where(['companyID' => $_GET['companyID']]);
        }

        if (isset($_GET['catID'])){
            $query->where(['catID' => $_GET['catID']]);
        }

        // add conditions that should always apply here

        $pages = new Pagination();
        $pages->pageSize = 5;

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => $pages,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            //'id' => $this->id,
            'catID' => $this->catID,
            'companyID' => $this->companyID,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'preview', $this->preview])
            ->andFilterWhere(['like', 'icon', $this->icon]);

        return $dataProvider;
    }
}
