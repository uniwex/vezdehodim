<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 *
 * @property integer $phone
 */
class SaleForm extends Model
{
    public $phone;
    public $price;
    public $qty;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['phone', 'qty'], 'required'],
            [['price'], 'required', 'message' => 'Необходимо выбрать билет'],
            [['phone'], 'match', 'pattern' => '/^([+]?[0-9 ]+)$/'],
            [['qty'], 'integer', 'min' => 1],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'phone' => 'Мобильный телефон',
            'price' => 'Билеты',
            'qty' => 'Количество',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     * @return bool whether the email was sent
     */
    /*public function sendEmail($email)
    {
        return Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom([$this->email => $this->name])
            ->setSubject($this->subject)
            ->setTextBody($this->body)
            ->send();
    }*/
}
