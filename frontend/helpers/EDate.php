<?php
namespace frontend\helpers;

use Yii;

class Edate
{
    public static function mounth($date)
    {
        switch ($date->format('m')){
            case 1: $ret = 'января'; break;
            case 2: $ret = 'февраля'; break;
            case 3: $ret = 'марта'; break;
            case 4: $ret = 'апреля'; break;
            case 5: $ret = 'мая'; break;
            case 6: $ret = 'июня'; break;
            case 7: $ret = 'июля'; break;
            case 8: $ret = 'августа'; break;
            case 9: $ret = 'сентября'; break;
            case 10: $ret = 'октября'; break;
            case 11: $ret = 'ноября'; break;
            case 12: $ret = 'декабря'; break;
        }
        return $ret;
    }

    public static function weekDay($date)
    {
        switch ($date->format('N')){
            case 1: $ret = 'понедельник'; break;
            case 2: $ret = 'вторник'; break;
            case 3: $ret = 'среда'; break;
            case 4: $ret = 'четверг'; break;
            case 5: $ret = 'пятница'; break;
            case 6: $ret = 'суббота'; break;
            case 7: $ret = 'воскресенье'; break;
        }
        return $ret;
    }

    public static function countDown($dateFrom, $dateTo)
    {

        return $dateFrom->diff($dateTo)->format('%d д. %h ч. %i м.');
    }
}