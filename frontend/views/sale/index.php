<?php
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use yii\helpers\ArrayHelper;

$this->title = 'Купить билет';
$this->registerJsFile('js/sale.js');
?>
<?php $form = ActiveForm::begin(); ?>
<div class="row">
    <div class="col-md-12">
<a href="/details/index?id=<?=$event->id?>"><?=$event->name?></a>
    </div>
</div>
<div class="sale-phone">
    <?= $form->field($model, 'phone')->widget(MaskedInput::className(), [
        'id' => 'sale-phone',
        'name' => 'phone',
        'mask' => '+7 999 999 9999',
        'clientOptions'=>[
            'removeMaskOnSubmit' => true,
        ],
    ]) ?>
    <?
    /*echo MaskedInput::widget([
        'model' => $model,
        'id' => 'sale-phone',
        'name' => 'phone',
        'mask' => '+7 999 999 9999',
        'clientOptions'=>[
            //'removeMaskOnSubmit' => true,
        ],
    ]);*/
    ?>
</div>
<div class="sale-price">
    <?= $form->field($model, 'price')->dropDownList(
            ArrayHelper::map($event->price,'id', 'nameValue'),
            [
                'prompt' => 'Выбрать билет',
                ''
            ]) ?>
</div>

<div class="sale-qty">
    <?= $form->field($model, 'qty')->textInput([
        'id' => 'qty',
        'value' => '1',
        'type' => 'number',
        'min' => 1,
        'options' => [
        ],
    ]) ?>

</div>

<div class="row">
    <div class="col-md-12 sale-ytype">
        <div id="ytype">
            <label for="ytype">Способ оплаты</label><br>
            <input id="ytypePC" name="ytype" value="PC" checked="" type="radio"> <label for="ytypePC">из кошелька в Яндекс.Деньгах</label><br>
            <input id="ytypeAC" name="ytype" value="AC" type="radio"> <label for="ytypeAC">с карты VISA или MasterCard</label><br>
            <input id="ytypeMC" name="ytype" value="MC" type="radio"> <label for="ytypeMC">с баланса сотового</label><br>
        </div>
    </div>
</div>
<div class="row ">
    <div class="col-md-12 sale-sum">
        Цена: <img src="/images/spinner.gif" id="sum-spinner" style="display: none;"><span id="sum-value">0</span> руб.
    </div>
</div>
    <input type="submit" value="Купить" class="btn btn-success">
<?php ActiveForm::end(); ?>
<? if (isset($url)) echo $url?>
