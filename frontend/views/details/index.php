<?php
use frontend\helpers\EDate;
use yii\widgets\Menu;
use yii\bootstrap\Carousel;
use dosamigos\gallery\Gallery;

/* @var $this yii\web\View */

$this->title = 'VEZDEHOD.ME';

?>

<div class="site-index">
    <?
    $date = new DateTime($model->eDate);
    $now = new DateTime('now');
    //echo $date->format('Y-m-d H:i:s');
    ?>
    <div class="event">
        <div class="row event-header">
            <div class="col-md-1 event-day">
                <?=$date->format('d')?>
            </div>
            <div class="col-md-2">

                <div class="row">
                    <div class="col-md-12 event-mounth">
                        <?=EDate::mounth($date)?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 event-weekDay">
                        (<?=EDate::weekDay($date)?>)
                    </div>
                </div>
            </div>

            <div class="col-md-5 event-time">
                <?=$date->format('H:i')?>
            </div>

            <div class="col-md-2 event-category">
                <a href="/?catID=<?=$model->category->id?>"><?=$model->categoryName?></a>
            </div>

            <div class="col-md-1 catImg">
                <a href="/?catID=<?=$model->category->id?>"><img src="/uploads/<?=$model->category->icon?>"></a>
            </div>

            <div class="col-md-1 ageLimit">
                <?=$model->ageLimit?>+
            </div>
        </div>

        <div class="row event-content">
            <div class="col-md-3 event-image">
                <?
                if ($model->icon)
                    $src = '/uploads/'.$model->icon;
                else
                    $src = '/images/noimage.png'
                ?>
                <img src="<?=$src?>">
            </div>

            <div class="col-md-9 event-content-text">
                <div class="row">
                    <div class="col-md-12 event-about">
                        <div class="row">
                            <div class="col-md-12 event-name">
                                <?=$model->name?>
                            </div>
                            <div class="col-md-12 event-preview">
                                <?=nl2br($model->preview)?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-8">

                        <div class="row">
                            <div class="col-md-12 event-company">
                                <img src="/images/company.png">
                                <?=$model->companyName?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 event-price">
                                <img src="/images/price.png">
                                <?=$model->priceRange?> руб.
                            </div>
                        </div>

                    </div>
                    <div class="col-md-4 event-countLeft">
                        осталось<br><span><?=$model->left?></span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row event-footer">

            <div class="col-md-3 countDown">
                <i class="glyphicon glyphicon-time"></i><br>
                <?=EDate::countDown($date, $now)?>
            </div>

            <div class="col-md-3 event-sale-ticket">
                поделиться
            </div>

            <div class="col-md-1 event-sale-ticket">
                <span class="glyphicon glyphicon-star"></span>
            </div>

            <div class="col-md-2 event-sale-ticket">
                <a href="/sale?id=<?=$model->id?>">купи другу</a>
            </div>

            <div class="col-md-3 event-sale-ticket">
                <a href="/sale?id=<?=$model->id?>">купи билет</a>
            </div>

        </div>
    </div>


    <div class="row">


            <?
            //$itemsImg = [];
            $itemsGallery = [];
            $display = false;
            foreach ($images as $image){
                //$itemsImg[] = '<img src="/uploads/'.$image->id.$image->ext.'">';
                $itemsGallery[] = [
                    'url' => 'http://'.$_SERVER['SERVER_NAME'].'/uploads/'.$image->id.$image->ext,
                    'src' => 'http://'.$_SERVER['SERVER_NAME'].'/uploads/'.$image->id.$image->ext,
                    'options' => ['style' => $display?'display: none;':''],
                ];
                $display = true;
            }
            ?>

        <div class="col-md-6">
            <?= Gallery::widget(['items' => $itemsGallery])?>
        </div>
        <div class="col-md-6">
            <?
            if ($model->youtube){
            $videoID = substr(parse_url($model->youtube)['path'],1);
            ?>

                    <iframe id="ytplayer" type="text/html" height="200"
                            src="http://www.youtube.com/embed/<?=$videoID?>?autoplay=0&origin=http://<?=$_SERVER['SERVER_NAME']?>"
                            frameborder="0" allowfullscreen></iframe>

            <? }?>
        </div>
    </div>
    <div class="row event-description">
            <div class="col-md-12">
                <?=$model->description?>
            </div>
    </div>
</div>
