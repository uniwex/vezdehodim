<?php
use yii\widgets\ListView;

/* @var $this yii\web\View */

$this->title = 'VEZDEHOD.ME';

?>
<div class="site-index">

    <?=ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_index',
        'summary' => '',
    ])?>

</div>
