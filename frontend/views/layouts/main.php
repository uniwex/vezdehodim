<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use common\models\Category;
use yii\widgets\Menu;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 header">
                HEADER
            </div>
        </div>
    </div>
    <div class="container main">
        <div class="row">
            <div class="col-md-2">
                <?
                $category = Category::find()->all();
                $items[] = ['label' => 'Все категории', 'url' => ['/']];

                foreach ($category as $item){
                if (isset($_GET['catID']))
                if ($item->id == $_GET['catID'])
                $items[] = [
                'label' => $item->icon?'<img src="/uploads/'.$item->icon.'">'.$item->name:$item->name,
                'url' => ['site/index', 'catID' => $item->id],
                //'active' => 'true',
                ];
                else
                $items[] = [
                'label' => $item->icon?'<img src="/uploads/'.$item->icon.'">'.$item->name:$item->name,
                'url' => ['site/index', 'catID' => $item->id],
                ];
                else
                $items[] = [
                'label' => $item->icon?'<img src="/uploads/'.$item->icon.'">'.$item->name:$item->name,
                'url' => ['site/index', 'catID' => $item->id],
                ];
                }

                ?>
                <?=
                Menu::widget([
                    'encodeLabels' => false,
                    'items' => $items,
                    'options'=>[
                        'class' => 'nav category-menu'
                    ],
                    //'activateParents'=>true,
                    //'activeCssClass'=>'active',
                ])
                ?>
            </div>
            <div class="col-md-10">
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
                <?= Alert::widget() ?>
                <?= $content ?>
            </div>
        </div>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; VEZDEHOD.ME <?= date('Y') ?></p>

        <p class="pull-right"><a href="http://red-flag.ru" target="_blank">Red Flag</a></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
