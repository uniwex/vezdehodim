<?php
return [
    '' => 'site/index',
    '<controller>' => '<controller>/index',
    '<action>' => 'site/<action>',
    'index' => 'site/index',
    'signup' => 'site/signup',
    'login' => 'site/login',
    'contact' => 'site/contact',
    'password-reset' => '/site/request-password-reset',
    'warranties' => 'site/warranties',
    /*    'faq' => 'site/faq',
        'personal/index/<token:[\w]+>' => 'personal/default/index',
        'personal/index' => 'personal/default/index',*/
    'details/<id:\d+>' => 'details/index',
    [
        'pattern' => 'order/index',
        'route' => 'order'
    ],
    [
        'pattern' => 'order/<token:[\w]+>',
        'route' => 'order/view'
    ],
    [
        'pattern' => 'news/index',
        'route' => 'news'
    ],
    [
        'pattern' => 'news/<slug:[\w-_]+>',
        'route' => 'news/view'
    ],
    [
        'pattern' => 'services/order/<slug:[\w-_]+>',
        'route' => 'services/service'
    ],
    [
        'pattern' => 'services/testturbo',
        'route' => 'services/testturbo'
    ],/*
    [
        'pattern' => 'services/testt',
        'route' => 'services/testt'
    ],*/
    [
        'pattern' => 'services/change-subscriber',
        'route' => 'services/change-subscriber'
    ],
    [
        'pattern' => 'services/index',
        'route' => 'services'
    ],
    [
        'pattern' => 'services/city',
        'route' => 'services/city'
    ],
    [
        'pattern' => 'personal/basket/delete',
        'route' => 'personal/basket/delete'
    ],
    [
        'pattern' => 'services/check-task',
        'route' => 'services/check-task'
    ],
    [
        'pattern' => 'services/<slug:\w+>',
        'route' => 'services/category'
    ],
    [
        'pattern' => 'stat/<slug:[\w\-]+>',
        'route' => 'static-page/view'
    ],
    [
        'pattern' => 'news/<slug:[\w\-]+>',
        'route' => 'news/view'
    ],
    [
        'pattern' => 'news/author/<author:[\w\-]+>',
        'route' => 'news/author'
    ],
];