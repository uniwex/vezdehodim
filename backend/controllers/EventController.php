<?php

namespace backend\controllers;

use Yii;
use common\models\Event;
use common\models\EventSearch;
use common\models\EventImg;
use common\models\Image;
use common\models\Company;
use common\models\Category;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;

/**
 * EventController implements the CRUD actions for Event model.
 */
class EventController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index', 'update', 'view', 'create', 'delete', 'photos', 'upload', 'deleteImg'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Event models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = null;
        if (isset($_GET['companyID'])){
            $model = Company::findOne($_GET['companyID']);
        }
        $searchModel = new EventSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $categoryList = ArrayHelper::map(Category::find()->all(),'id','name');
        $companyList = ArrayHelper::map(Company::find()->all(),'id','name');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
            'categoryList' => $categoryList,
            'companyList' => $companyList,
        ]);
    }

    /**
     * Displays a single Event model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Event model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Event();
        $company = Company::find()->all();
        $category = Category::find()->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (isset($_GET['companyID'])){
                return $this->redirect(['index', 'companyID' => $_GET['companyID']]);
            }
            else{
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'company' => $company,
                'category' => $category,
            ]);
        }
    }

    /**
     * Updates an existing Event model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $company = Company::find()->all();
        $category = Category::find()->all();
        $filename = null;
        if (isset($_FILES['Event']))
            if ($_FILES['Event']['error']['icon'] == 0){
                $uploadfile = '';
                $img = $_FILES['Event'];
                $uploaddir = $_SERVER['DOCUMENT_ROOT'].'frontend/web/uploads/';
                $file = pathinfo($img['name']['icon']);
                $filename = time().'.'.$file['extension'];
                $uploadfile = $uploaddir . basename($filename);
                move_uploaded_file($img['tmp_name']['icon'], $uploadfile);
            }

        if ($model->load(Yii::$app->request->post())) {
            if (isset($_FILES['Event']))
                if ($_FILES['Event']['error']['icon'] == 0)
                    $model->icon = $filename;
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
            else {
                echo '<pre>';
                print_r($_POST);
                echo '<pre>';
                /*return $this->render('update', [
                    'model' => $model,
                    'company' => $company,
                    'category' => $category,
                ]);*/
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'company' => $company,
                'category' => $category,
            ]);
        }
    }

    /**
     * Deletes an existing Event model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Event model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Event the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */

    public function actionPhotos($id)
    {
        $images = EventImg::find()->where(['eventID' => $id])->joinWith('image')->all();
        $event = Event::findOne($id);
        return $this->render('photos',[
            'eventID' => $id,
            'event' => $event,
            'images' => $images,
        ]);
    }

    public function actionUpload()
    {
        if (isset($_POST) and isset($_FILES)){
            $upload_dir = '/var/www/tickets/frontend/web/uploads/';
            $filename = $_FILES['img']['name'];
            $ext = substr($filename, strpos($filename,'.'), strlen($filename)-1);
            $transaction = Image::getDb()->beginTransaction();
            try{
                $img = new Image();
                $img->name = $_FILES['img']['name'];
                //$img->size = $_FILES['img']['size'];
                $img->ext = $ext;
                $img->save();
                $imgID = $img->id;
                $eventImg = new EventImg();
                $eventImg->imgID = $imgID;
                $eventImg->eventID = $_POST['eventID'];
                $eventImg->save();
                $upload_file = $upload_dir . basename($imgID.$ext);
                move_uploaded_file($_FILES['img']['tmp_name'], $upload_file);
                $transaction->commit();
            }
            catch(\Exception $e) {
                $transaction->rollBack();
                throw $e;
            } catch(\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }

        }
        return $this->renderPartial('upload');
    }

    public function actionDeleteImg()
    {
        if (isset($_POST)){
            $transaction = Image::getDb()->beginTransaction();
            try{
                $image = Image::findOne($_POST['key']);
                $file = '/var/www/tickets/frontend/web/uploads/'.$_POST['key'].$image->ext;
                EventImg::findOne(['eventID' => $_POST['eventID'], 'imgID' => $_POST['key']])->delete();
                $image->delete();
                unlink($file);
                $transaction->commit();
            }
            catch(\Exception $e) {
                $transaction->rollBack();
                throw $e;
            } catch(\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }
        }
        return $this->renderPartial('deleteImg');
    }

    protected function findModel($id)
    {
        if (($model = Event::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
