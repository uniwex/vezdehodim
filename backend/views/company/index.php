<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\CompanySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Компании';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Новая компания', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

         //   'id',
            'name',
            'description',
            'preview',
            'iconPath:image',
            'username',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{list} {view} {update} {delete} ',
                'buttons' => [
                    'list' => function ($url,$model) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-list"></span>',
                            '/admin/event/index/?companyID='.$model->id);
                    },
                ],
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
