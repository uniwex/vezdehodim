<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\Company */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="company-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?php
    if ($model->icon != null)
        $initialPreview = ['http://'.$_SERVER['SERVER_NAME'].'/uploads/'.$model->icon];
    else
        $initialPreview = '';
    ?>
    <?= $form->field($model, 'icon')->widget(FileInput::classname(), [
        'name' => 'attachment',
        'pluginOptions' => [
            'showCaption' => false,
            'showRemove' => false,
            'showUpload' => false,
            'browseClass' => 'btn btn-primary btn-block',
            'browseIcon' => '<i class="glyphicon glyphicon-camera"></i>',
            'browseLabel' =>  'Выбрать изображение',
            'initialPreview' => $initialPreview,
            'initialPreviewAsData'=>true,
        ],
        'options' => ['accept' => 'image/*'],
    ]) ?>

    <?= $form->field($model, 'icon')->hiddenInput()->label(false); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['maxlength' => true]); ?>

    <?= $form->field($model, 'preview')->textarea(['maxlength' => true]) ?>

    <?= $form->field($model, 'userID')->dropDownList(ArrayHelper::map($user,'id', 'username')) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
