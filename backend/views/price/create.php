<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Price */

$this->title = 'Новая цена';
$this->params['breadcrumbs'][] = ['label' => 'Цены', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="price-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'event' => $event,
    ]) ?>

</div>
