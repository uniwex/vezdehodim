<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\PriceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = 'Цены';
$this->params['breadcrumbs'][] = ['label' => 'События', 'url' => ['event/index']];
if (isset($_GET['eventID'])){
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['event/view', 'id' => $model->id]];
}
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="price-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?
        if (isset($_GET['eventID'])){
            echo Html::a('Новая цена', ['create?eventID='.$_GET['eventID']], ['class' => 'btn btn-success']);
        }
        else{
            echo Html::a('Новая цена', ['create'], ['class' => 'btn btn-success']);
        }
        ?>

    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'companyName',
            'eventName',
         //   'id',
            'name',
            'qty',
            'sold',
            'left',
            'value',
            'iconPath:image',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{list} {view} {update} {delete} ',
                'buttons' => [
                    'list' => function ($url,$model) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-list"></span>',
                            '/admin/sold/index/?id='.$model->id);
                    },
                ],
            ],
        ],
    ]); ?>
</div>
