<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\EventSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'События';
if (isset($_GET['companyID'])){
    $this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['company/view', 'id' => $model->id]];
}
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?
        if (isset($_GET['companyID'])){
            echo Html::a('Новое событие', ['create?companyID='.$_GET['companyID']], ['class' => 'btn btn-success']);
        }
        else{
            echo Html::a('Новое событие', ['create'], ['class' => 'btn btn-success']);
        }
        ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{list} {photos} {view} {update} {delete} ',
                'buttons' => [
                    'list' => function ($url,$model) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-list"></span>',
                            '/admin/price/index/?eventID='.$model->id);
                    },
                    'photos' => function($action, $key, $index) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-camera">',
                            Url::to(['event/photos', 'id' => $index])
                        );
                    },
                ],
            ],
            [
                'attribute'=>'companyID',
                'label'=>'Компания',
                'format'=>'text',
                'content'=>function($data){
                    return $data->companyName;
                },
                'filter' => $companyList,
            ],
            [
                'attribute'=>'catID',
                'label'=>'Категория',
                'format'=>'text',
                'content'=>function($data){
                    return $data->categoryName;
                },
                'filter' => $categoryList,
            ],
            //'id',
            'name',
            'preview',
            'description:html',
            'ageLimit',
            'eDate:datetime',
            'iconPath:image',
            'youtube',
            // 'companyID',
        ],
    ]); ?>
</div>
