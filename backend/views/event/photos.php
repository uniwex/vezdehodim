<?
use yii\helpers\Url;
use yii\helpers\Html;
use kartik\file\FileInput;

$this->title = $event->name.': изображения';
?>
    <h1><?= Html::encode($this->title) ?></h1>
<?
$i = 0;
$path = 'http://'.$_SERVER['SERVER_NAME'].'/uploads/';
$initialPreview = [];
$initialPreviewConfig = [];

//if ($images->count() >= 0) {
//    $images->all();
foreach ($images as $img) {
    $initialPreview[$i] = $path . $img->imgID . $img->image->ext;
    $initialPreviewConfig[$i] = [/*'caption' => $img->image->caption, 'size' => $img->image->size,*/ 'key' => $img->imgID];
    $i++;
}
//}

echo FileInput::widget([
    'name' => 'img',
    'options'=>[
        'multiple'=>true
    ],
    'pluginOptions' => [
        'allowedFileExtensions'=>['jpg', 'gif', 'png', 'bmp'],
        'initialPreview'=>$initialPreview,
        'initialPreviewAsData'=>true,
        'initialCaption'=>"Загруженное",
        'initialPreviewConfig' => $initialPreviewConfig,
        'uploadUrl' => Url::to(['/event/upload']),
        'deleteUrl' => Url::to(['/event/delete-img']),
        'uploadExtraData' => [
            'eventID' => $eventID,
        ],
        'deleteExtraData' => [
            'eventID' => $eventID,
        ],
        'maxFileCount' => 10,
        'overwriteInitial'=>false,
    ]
]);
?>