<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use dosamigos\datetimepicker\DateTimePicker;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model common\models\Event */
/* @var $form yii\widgets\ActiveForm */

$ageLimit = [
    '0' => '0+',
    '6' => '6+',
    '12' => '12+',
    '18' => '18+',
    ];

if (isset($_GET['companyID'])){
    $model->companyID = $_GET['companyID'];
}
?>

<div class="event-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?php
    if ($model->icon != null)
        $initialPreview = ['http://'.$_SERVER['SERVER_NAME'].'/uploads/'.$model->icon];
    else
        $initialPreview = '';
    ?>
    <?= $form->field($model, 'icon')->widget(FileInput::classname(), [
        'name' => 'attachment',
        'pluginOptions' => [
            'showCaption' => false,
            'showRemove' => false,
            'showUpload' => false,
            'browseClass' => 'btn btn-primary btn-block',
            'browseIcon' => '<i class="glyphicon glyphicon-camera"></i>',
            'browseLabel' =>  'Выбрать изображение',
            'initialPreview' => $initialPreview,
            'initialPreviewAsData'=>true,
        ],
        'options' => ['accept' => 'image/*'],
    ]) ?>

    <?= $form->field($model, 'icon')->hiddenInput()->label(false); ?>

    <?= $form->field($model, 'companyID')->dropDownList(ArrayHelper::map($company,'id', 'name')) ?>

    <?= $form->field($model, 'catID')->dropDownList(ArrayHelper::map($category,'id', 'name')) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['maxlength' => true])->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'standart',
    ]) ?>

    <?= $form->field($model, 'preview')->textarea(['maxlength' => true]) ?>

    <?= $form->field($model, 'eDate')->widget(DateTimePicker::className(), [
        'language' => 'ru',
        /*'size' => 'ms',
        'template' => '{input}',*/
        'pickButtonIcon' => 'glyphicon glyphicon-time',
        //'inline' => true,
        'clientOptions' => [
            /*'startView' => 1,
            'minView' => 0,
            'maxView' => 1,*/
            'autoclose' => true,
            //'linkFormat' => 'yyyy-mm-dd HH:ii', // if inline = true
            'format' => 'yyyy-mm-dd HH:ii',
            'pickerPosition' => 'bottom-left',
        ]
    ])?>

    <?= $form->field($model, 'ageLimit')->dropDownList($ageLimit) ?>

    <?= $form->field($model, 'youtube')->textInput(['maxlength' => true]) ?>



    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?
//$this->registerJs("CKEDITOR.plugins.addExternal('pbckcode', '/js/ckeditor.js', '');");
?>