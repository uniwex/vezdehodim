<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?php
    if ($model->icon != null)
        $initialPreview = ['http://'.$_SERVER['SERVER_NAME'].'/uploads/'.$model->icon];
    else
        $initialPreview = '';
    ?>
    <?= $form->field($model, 'icon')->widget(FileInput::classname(), [
        'name' => 'attachment',
        'pluginOptions' => [
            'showCaption' => false,
            'showRemove' => false,
            'showUpload' => false,
            'browseClass' => 'btn btn-primary btn-block',
            'browseIcon' => '<i class="glyphicon glyphicon-camera"></i>',
            'browseLabel' =>  'Выбрать изображение',
            'initialPreview' => $initialPreview,
            'initialPreviewAsData'=>true,
        ],
        'options' => ['accept' => 'image/*'],
    ]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
