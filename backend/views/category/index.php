<?php

use yii\helpers\Html;
use leandrogehlen\treegrid\TreeGrid;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->registerJsFile('/admin/js/category.js');
$this->title = 'Категории';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">

    <p>
        <?php
        //if ($dataProvider->count == 0)
            echo Html::a('Создать корневой элемент', ['add'], ['class' => 'btn btn-success']);
        ?>
    </p>
    <?=
    TreeGrid::widget([
        'dataProvider' => $dataProvider,
        'keyColumnName' => 'id',
        'showOnEmpty' => false,
        'parentColumnName' => 'parentID',
        'columns' => [

            'name',
            'iconPath:image',
            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete} {add}',
                'buttons' => [
                    'view' => function ($url, $model, $key)
                    {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',
                            '/admin/category/view?id='.$model->id);
                    },
                    'update' => function ($url, $model, $key)
                    {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                            '/admin/category/update?id='.$model->id);
                    },
                    'delete' => function ($url, $model, $key)
                    {
                        /*return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                            '/admin/category/delete?id='.$model->id);*/
                        return '<a href="/admin/category/delete?id='.$model->id.'" title="Удалить" aria-label="Удалить" data-pjax="0" data-confirm="Вы уверены, что хотите удалить этот элемент?" data-method="post"><span class="glyphicon glyphicon-trash"></span></a>';
                    },
                    'add' => function ($url, $model, $key)
                    {
                        return Html::a('<span class="glyphicon glyphicon-plus"></span>',
                                        '/admin/category/add?id='.$model->id);
                    },
                ]
            ],
        ]
    ]);
    ?>

</div>
