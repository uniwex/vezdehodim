<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\helpers\PhoneFormat;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $price->event->name.': '.$price->name;
$this->params['breadcrumbs'][] = ['label' => 'События', 'url' => ['event/index']];
$this->params['breadcrumbs'][] = ['label' => $price->event->name, 'url' => ['price/index', 'eventID' => $price->event->id]];
$this->params['breadcrumbs'][] = $price->name;
?>
<div class="sold-tickets-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Новый билет', ['create?id='.$_GET['id']], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            //'priceID',
            'hash',
            [
                'attribute'=>'phone',
                'label'=>'Телефон',
                'format'=>'text',
                'content'=>function($data){
                    return PhoneFormat::format($data->phone);
                },
            ],
            'priceValue',
            'amountValue',
            // 'phone',
            // 'updated_at',
            // 'created_at',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete} ',
                'buttons' => [
                    'update' => function ($url,$model) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-pencil"></span>',
                            '/admin/sold/update/?id='.$model->id.'&priceID='.$model->price->id);
                    },
                ],
            ],
        ],
    ]); ?>
</div>
