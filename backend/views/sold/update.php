<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SoldTickets */

$this->title = 'Билет ' . $model->id." (хеш: $model->hash)";
$this->params['breadcrumbs'][] = ['label' => 'События', 'url' => ['event/index']];
$this->params['breadcrumbs'][] = ['label' => $price->event->name, 'url' => ['price/index', 'eventID' => $price->event->id]];
$this->params['breadcrumbs'][] = ['label' => $price->name, 'url' => ['sold/index', 'id' => $price->id]];
$this->params['breadcrumbs'][] = 'Билет '.$model->id.': редактирование';
?>
<div class="sold-tickets-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
