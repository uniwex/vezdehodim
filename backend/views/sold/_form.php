<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model common\models\SoldTickets */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sold-tickets-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'priceID')->hiddenInput()->label(false); ?>

    <?= $form->field($model, 'hash')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->widget(MaskedInput::className(), [
        'id' => 'sale-phone',
        'name' => 'phone',
        'mask' => '+7 999 999 9999',
        'clientOptions'=>[
            'removeMaskOnSubmit' => true,
        ],
    ]) ?>

    <?= $form->field($model, 'priceValue')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'amountValue')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
