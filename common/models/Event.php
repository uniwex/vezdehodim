<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "event".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $preview
 * @property string $icon
 * @property integer $companyID
 * @property integer $catID
 * @property integer $eDate
 */
class Event extends \yii\db\ActiveRecord
{
    public $iconDir = '/uploads/';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'companyID', 'catID', 'eDate', 'ageLimit'], 'required'],
            [['companyID', 'catID', 'ageLimit'], 'integer'],
            //['eDate', 'datetime', 'format' => 'yyyy-mm-dd HH:ii'],
            [['name', 'preview', 'icon', 'youtube'], 'string', 'max' => 255],
            [['description'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'description' => 'Описание',
            'preview' => 'Краткое описание',
            'icon' => 'Иконка',
            'iconPath' => 'Иконка',
            'companyID' => 'Компания',
            'catID' => 'Категория',
            'categoryName' => 'Категория',
            'companyName' => 'Компания',
            'eDate' => 'Дата',
            'ageLimit' => 'Возрастное ограничение',
            'youtube' => 'Ссылка на youtube',
        ];
    }

    public function getCompany()
    {
        return $this->hasOne(Company::className(),['id' => 'companyID']);
    }

    public function getCategory()
    {
        return $this->hasOne(Category::className(),['id' => 'catID']);
    }

    public function getImage()
    {
        return $this->hasMany(Image::className(), ['id' => 'imgID'])
            ->viaTable('eventImg', ['eventID' => 'id']);
    }

    public function getCompanyName()
    {
        return $this->company->name;
    }

    public function getCategoryName()
    {
        return $this->category->name;
    }

    public function getIconPath()
    {
        if ($this->icon == null)
            return null;
        else
            return $this->iconDir.$this->icon;
    }

    public function getPrice()
    {
        return $this->hasMany(Price::className(), ['eventID' => 'id']);
    }

    public function getPriceRange()
    {
        $min = Price::find()->where(['eventID' => $this->id])->min('value');
        $max = Price::find()->where(['eventID' => $this->id])->max('value');
        if ($min == $max)
            return "$min";
        else
            return "$min - $max";
    }

    public function getLeft()
    {
        $prices = $this->price;
        $left = 0;
        foreach($prices as $price){
            $left += SoldTickets::find()->where(['priceID' => $price->id])->count();
        }
        return Price::find()->where(['eventID' => $this->id])->sum('qty') - $left;
    }
}
