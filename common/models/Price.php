<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use common\models\SoldTickets;

/**
 * This is the model class for table "price".
 *
 * @property integer $id
 * @property string $name
 * @property integer $qty
 * @property string $icon
 * @property integer $eventID
 * @property float $value
 * @property float $sold
 * @property float $left
 */
class Price extends \yii\db\ActiveRecord
{
    public $iconDir = '/uploads/';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'price';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'qty', 'eventID', 'value'], 'required'],
            [['qty', 'eventID'], 'integer'],
            [['value'],'double'],
            [['name', 'icon'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'icon' => 'Иконка',
            'iconPath' => 'Иконка',
            'qty' => 'Количество',
            'value' => 'Значение',
            'eventID' => 'Событие',
            'eventName' => 'Событие',
            'companyName' => 'Компания',
            'sold' => 'Продано',
            'left' => 'Осталось',
        ];
    }

    public function getEvent()
    {
        return $this->hasOne(Event::className(),['id' => 'eventID']);
    }

    public function getCompany()
    {
        /*return $this->hasOne(Company::className(), ['eventID' => 'id'])
            ->viaTable('event', ['companyID' => 'id']);*/

        return $this->hasOne(Company::className(), ['id' => 'companyID'])
            ->viaTable('event', ['id' => 'eventID']);
    }

    public function getImage()
    {
        return $this->hasMany(Image::className(), ['id' => 'imgID'])
            ->viaTable('priceImg', ['priceID' => 'id']);
    }

    public function getEventName()
    {
        return $this->event->name;
    }

    public function getCompanyName()
    {
        return $this->event->company->name;
    }

    public function getIconPath()
    {
        if ($this->icon == null)
            return null;
        else
            return $this->iconDir.$this->icon;
    }

    public function getSold()
    {
        return SoldTickets::find()->where(['priceID' => $this->id])->count();
    }

    public function getLeft()
    {
        return $this->qty - $this->sold;
    }

    public function getNameValue()
    {
        return $this->name.' - '.$this->value.' руб. (осталось '.$this->left.')';
    }
}
