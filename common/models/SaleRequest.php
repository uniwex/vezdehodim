<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "saleRequest".
 *
 * @property integer $id
 * @property integer $paymentID
 * @property string $request
 */
class SaleRequest extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'saleRequest';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['paymentID'], 'required'],
            [['paymentID'], 'integer'],
            [['request'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'paymentID' => 'Payment ID',
            'request' => 'Request',
        ];
    }
}
