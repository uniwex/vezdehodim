<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "company".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $preview
 * @property string $icon
 * @property integer $userID
 */
class Company extends \yii\db\ActiveRecord
{
    public $iconDir = '/uploads/';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'userID'], 'required'],
            [['userID'], 'integer'],
            [['name', 'description', 'preview', 'icon'], 'string', 'max' => 255],
            [['icon'], 'image', 'extensions' => 'png, jpg, jpeg, gif', 'maxFiles' => 4, 'skipOnEmpty' => true],
            [['icon'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'description' => 'Описание',
            'preview' => 'Краткое описание',
            'icon' => 'Иконка',
            'iconPath' => 'Иконка',
            'userID' => 'User ID',
            'username' => 'Пользователь',
        ];
    }

    public function upload($dir, $imageObj) {
        $name = $imageObj->baseName;
        $ext = $imageObj->extension;
        $imageObj->saveAs($dir . $name . '.' . $ext);
        return true;
    }

    public function getUser()
    {
        return $this->hasOne(User::className(),['id' => 'userID']);
    }

    public function getImage()
    {
        return $this->hasMany(Image::className(), ['id' => 'imgID'])
            ->viaTable('companyImg', ['companyID' => 'id']);
    }

    public function getUsername()
    {
        return $this->user->username;
    }

    public function getIconPath()
    {
        if ($this->icon == null)
            return null;
        else
            return $this->iconDir.$this->icon;
    }
}
