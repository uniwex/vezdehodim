<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use common\models\Price;

/**
 * This is the model class for table "soldTickets".
 *
 * @property integer $id
 * @property integer $priceID
 * @property string $hash
 * @property string $phone
 * @property string $priceValue
 * @property string $amountValue
 */
class SoldTickets extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'soldTickets';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['priceID', 'hash', 'phone', 'priceValue', 'amountValue'], 'required'],
            [['priceID'], 'integer'],
            [['phone'], 'match', 'pattern' => '/^([+]?[0-9 ]+)$/'],
            [['hash'], 'string', 'max' => 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'priceID' => 'Price ID',
            'hash' => 'Hash',
            'phone' => 'Телефон',
            'priceValue' => 'Цена',
            'amountValue' => 'Оплачено',
        ];
    }

    public function generateHash($id)
    {
        $expr = true;
        $hash = 'null';
        $price = Price::findOne($id);
        while($expr) {
            $random = rand(0, 10000) . time();
            $hash = substr(md5($random), 0, 5);
            $expr = $this::find()
                ->joinWith(['price p'])
                ->where(['hash' => $hash, 'p.eventID' => $price->eventID])->exists();
            //todo добавить условие по событию
            }
        return $hash;
    }

    public function getPrice()
    {
        return $this->hasOne(Price::className(),['id' => 'priceID']);
    }
}
