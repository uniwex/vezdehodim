<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "eventImg".
 *
 * @property integer $id
 * @property integer $eventID
 * @property integer $imgID
 */
class EventImg extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'eventImg';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['eventID', 'imgID'], 'required'],
            [['eventID', 'imgID'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'eventID' => 'Event ID',
            'imgID' => 'Img ID',
        ];
    }

    public function getImage()
    {
        return $this->hasOne(Image::className(),['id' => 'imgID']);
    }
}
