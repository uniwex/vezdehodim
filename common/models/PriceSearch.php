<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;

/**
 * PriceSearch represents the model behind the search form about `common\models\Price`.
 */
class PriceSearch extends Price
{
    public $eventName;
    public $companyName;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'qty', 'eventID'], 'integer'],
            [['name', 'eventName', 'companyName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Price::find();
        if (isset($_GET['eventID'])){
            $query->where(['eventID' => $_GET['eventID']]);
        }

        // add conditions that should always apply here

        $pages = new Pagination();
        $pages->pageSize = 5;

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => $pages,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            $query->joinWith(['company', 'event']);
            return $dataProvider;
        }

        // grid filtering conditions

        $query->joinWith(['event' => function ($q) {
                $q->joinWith(['company' => function ($q) {
                $q->where('company.name LIKE "%' . $this->companyName . '%" ');
            }]);
            $q->where('event.name LIKE "%' . $this->eventName . '%" ');
        }]);


        $query->andFilterWhere([
            'id' => $this->id,
            'qty' => $this->qty,
            'eventID' => $this->eventID,
        ]);


        $query->andFilterWhere(['like', 'price.name', $this->name]);


        return $dataProvider;
    }
}
