<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property string $name
 * @property string $parentID
 * @property string $icon
 */
class Category extends \yii\db\ActiveRecord
{
    public $iconDir = '/uploads/';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name', 'icon'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'icon' => 'Иконка',
            'iconPath' => 'Иконка',
        ];
    }

    public function getIconPath()
    {
        if ($this->icon == null)
            return null;
        else
            return $this->iconDir.$this->icon;
    }
}
