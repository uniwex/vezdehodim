<?php
namespace common\helpers;

use Yii;

class PhoneFormat
{
    public static function format($ph)
    {
        return '+7 (' . $ph[0] . $ph[1] . $ph[2] . ') ' . $ph[3] . $ph[4] . $ph[5] . ' ' . $ph[6] . $ph[7] . $ph[8] . $ph[9];
    }
}